---
title: Laboratorio de Redes Sociales 
subtitle: objetivos / intereses 
comments: TRUE
bigimg: [{src: "/img/city.jpg", desc: "city"}]
---


Esta *página web* tiene como objetivo ser un repositorio de proyectos de investigación, material pedagógico y cursos de investigadores(as) asociados al proyecto FONDECYT 1171426 y al proyecto MINICOES "Patrones de homofilia y heterofilia en las redes personales en perspectiva longitudinal: Análisis con datos ELSOC 2016-2019". En esta podrás encontrar modelos análisis, códigos reproducibles y adelantos de investigación. 

A mediano plazo nuestros objetivos e intereses son: 

*Nuestros objetivos*

- Establecer un grupo excelencia para el análisis de redes sociales en Chile
- Proveer análisis de redes sobre problemas sustantivos de la realidad social
- Estimular el interes con respecto al análisis de redes en Chile y Latinoamérica
- Hacer contribuciones importantes a la literatura de análisis de redes sociales


*Nuestros intereses*

- Métodos descriptivos y visualización
- Modelización estadística de redes sociales
- Métodos mixtos para redes sociales
- Redes sociales y movimientos sociales
- Redes sociales y preferencias distributivas
- Redes sociales, desigualdades y estratificación
- Análisis longitudinal de redes


## Recomendamos

*Libros* que introducen la practica de la *Open Science*
- [Investigación Social Abierta](https://juancarloscastillo.github.io/in-socabi/), por Juan Carlos Castillo. 
- [Open Science Training Handbook](https://open-science-training-handbook.gitbook.io/book/)

*Archivo* 
- [SocArchiv](https://socopen.org/welcome/)

*Documental* que reflexiona sobre los obejtivos de la perspectiva *Open Science* 
{{< vimeo 273358286 >}}

