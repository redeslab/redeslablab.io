---
title: "Calculo índice de Blau"
author: "Guillermo Beck"
date: "2020-07-29"
update: "2020-07-30"
tags: ["Redes Sociales", "Redes Egocentricas", "Generador de posiciones", "R"]

---


## Índice de Blau

El índice de Blau, conodido también como D de Simpson, fue propuesto incialmente por Simpson (1949) con el objetivo de medir diversidad de especies en ecosistemas (Harrison y Klein, 2007). Sin embargo, su aplicación el ámbito de ciencias sociales como medida de diversidad o heterogeneidad fue popularizada por Blau por lo que en este campo es más conocido como Índice de Blau (ver Blau, 1977; Rushton, 2008; Biemann y Kearney, 2010). De acuerdo a McDonald y Dimmick (2003), quienes testean una serie de índices para calcular diversidad, este posee una correlación sobre o superior a 0.95 con otras medidas similares, lo cual permite utilizar esta medida como un índice confiable para la estimación de diversidad.

La fórmula que representa el índice de Blau es


$$D= 1-\sum p_i^2 (1)$$


donde $$D$$ representa el índice de blau el cual es igual a 1 menos la sumatoria de la proporción de sujetos en cada categoría elevada al cuadrado, como se ve en la siguiente ecuación (1), donde, $$p_i^2$$ es la proporción en una determinada categoría, de tal modo que, para valores más cercanos a uno, el estadístico indica mayor presencia de heterogeneidad.

## Datos a utilizar

Para el cálculo de esta medida, utilizaremos la base de datos longitudinal, ELSOC (ola 2016), en formato R la cual puede ser descargada en el siguiente link <https://www.dataverse.harvard.edu/file.xhtml?persistentId=doi:10.7910/DVN/JDJLPQ/CJJV3T&version=3.0>.

Lo invitamos también a poder revisar el repositorio <https://www.dataverse.harvard.edu/dataverse/coes_data_repository> donde podrá encontrar disponible los datos de 2017, 2018 de la encuesta ELSOC en diversos formatos, así como también su cuestionario. En este link usted podrá también podrá encontrar las bases de datos corresponientes a otras encuestas realizadas por el centro. 

Para más información de COES o ELSOC, lo invitamos a revisar <https://www.coes.cl/> y <https://www.elsoc.cl/> 

## Cálculo del índice de Blau

#### 1) Cargar librerías y base de datos

Para este procedimiento utilizamos la librería "dplyr" y la base elsoc_2016 

Antes de comenzar, recuerde: 

- Instalar las librerías utilizadas. Para ello utilice el comando install.packges de R.

- Asegurese de modificar la ruta donde tiene alojado el archivo de extensión .RData ("C:/Users/.../ELSOC_W01_v4.00_R.RData")

```
#### Para instalaar librería descomente la linea siguiente y ejecute.
# install.packages("dplyr")


#### Cargar libreria y base de elsoc_2016

library(dplyr)
load("C:/Users/gmobe/Downloads/ELSOC_W01_v4.00_R.RData")

```

#### 2) Selección de variables y depuración

El índice de blau será calculado con los ítems del generador de posiciones codificados bajo el prefijo (r01), el cual pregunta al entrevistado por 13 ocupaciones de diverso prestigio ocupacional. Estas son:

Gerente empresa multinacional, Vendedor ambulante, Secretario, Mecanico de autos, Vendedor de tienda, Abogado , Aseador , Medico , Parvulario , Chofer de taxi,
Camarero, Contador y Profesor Universitario.

Los ítems del generador de posicionnes son variables ordinales de 7 categorías que consulta la cantidad de conocidos del entrevistado en cada ocupación.

```
Blau <-dplyr::select(elsoc_2016, 
                     starts_with("r01")
)


  Blau[Blau == -999]<-NA
  Blau[Blau == -888]<-NA
  
summary(Blau)

```

#### 3) Calculo

Dado que las variables son categorías, se genera variables numérica a las que se le imputa el valor medio a cada categoría. Ej. Si el entrevistado dice que conoce entre 5 y 7 abogados, el valor imputado será 7

```
#### Asignar valor medio a cada categoría de respuesta
x<-c(0,1,3,6,9,13,16) 

for(i in 1:nrow(Blau)){
  for(j in 2:ncol(Blau)){
    Blau[i,j]=x[Blau[i,j]]
  }
}

#### Se asigna el valor 0 cuando la celda tiene valor NA
Blau[is.na(Blau)] <-0

```

Se calcula el tamaño de la red total y se guardar en una variable (size) y se filtran los sujetos que poseen 0 conocidos en su red.

```
#Sumar el cálculo
Blau$size<-rowSums(Blau[,1:13])

#Hay que eliminar casos con valor 0
Blau<-Blau%>% 
  filter(size>0)
```

Se genera la proporción de conocidos en cada para ocupación 

$$p_i^2$$ 

```
Blau$p01_01<-(Blau$r01_01/Blau$size)*(Blau$r01_01/Blau$size)
Blau$p01_02<-(Blau$r01_02/Blau$size)*(Blau$r01_02/Blau$size)
Blau$p01_03<-(Blau$r01_03/Blau$size)*(Blau$r01_03/Blau$size)
Blau$p01_04<-(Blau$r01_04/Blau$size)*(Blau$r01_04/Blau$size)
Blau$p01_05<-(Blau$r01_05/Blau$size)*(Blau$r01_05/Blau$size)
Blau$p01_06<-(Blau$r01_06/Blau$size)*(Blau$r01_06/Blau$size)
Blau$p01_07<-(Blau$r01_07/Blau$size)*(Blau$r01_07/Blau$size)
Blau$p01_08<-(Blau$r01_08/Blau$size)*(Blau$r01_08/Blau$size)
Blau$p01_09<-(Blau$r01_09/Blau$size)*(Blau$r01_09/Blau$size)
Blau$p01_10<-(Blau$r01_10/Blau$size)*(Blau$r01_10/Blau$size)
Blau$p01_11<-(Blau$r01_11/Blau$size)*(Blau$r01_11/Blau$size)
Blau$p01_12<-(Blau$r01_12/Blau$size)*(Blau$r01_12/Blau$size)
Blau$p01_13<-(Blau$r01_13/Blau$size)*(Blau$r01_13/Blau$size)

```

Se estima Blau para cada sujeto de la red 

$$D= 1-\sum p_i^2 (1)$$

```
Blau$Blau=1-rowSums(Blau[,15:27])
```

#### 4) Visualizacion

```
hist.Blau<-hist(Blau$Blau)

jpeg("Blau.jpg")
plot(hist.Blau<-hist(Blau$Blau))
dev.off()
```


![Blau](http://imgfz.com/i/26b84rk.jpeg) 

#### 5) Bibliografia

- Biemann, T., & Kearney, E. (2010). Size Does Matter: How Varying Group Sizes in a Sample Affect the Most Common Measures of Group Diversity. Organizational Research Methods, 13(3), 582–599. https://doi.org/10.1177/1094428109338875
- Blau, P. M. (1977). Inequality and heterogeneity: A primitive theory of social structure (Vol. 7). New York: Free Press.
- Harrison, D. A., & Klein, K. J. (2007). What's the difference? Diversity constructs as separation, variety, or disparity in organizations. Academy of management review, 32(4), 1199-1228.
- McDonald, D. G. & Dimmick, J. (2003). “The Conceptualization and Measurement of Diversity”, Communication Research,
30(1), 60-69.
- Rushton, M. (2008). A note on the use and misuse of the racial diversity index. Policy Studies Journal, 36(3), 445-459.
- Simpson, E. (1949), Measurement of Diversity. Nature 163, 688. https://doi.org/10.1038/163688a0
