---
title: "Calculo índice EI (Manual)"
author: "Roberto Cantillan Carrasco"
date: "2021-05-09"
update: "2021-05-09"
tags: ["Redes Sociales", "Redes Egocentricas", "Generador de nombres", "R"]
---

<details><summary>ñlmlkmlkmlkmlknm</summary>

</details>

En el presente documento se reali``za un trabajo de datos para construir una base en formato long con la encuesta COES en su ola w2 y ola w4. Adicionalmente, se analizan datos panel con el modelo within-between (Bell & Jhones, 2014; Bell et al. 2019), ideales para el análisis de estructuras jerárquicas de datos, incluidos los datos datos de series de tiempo (de corte transversal), y de tipo panel. El objetivo es explorar las posibles relaciones entre indicadores de homofilia y algunos indicadores de comportamiento cívico. 

ara atributos categóricos de los alter de la red ego, además de la proporción de alter similares a ego, una medida generalmente usada ha sido el índice EI (Krackhardt & Stern, 1988, Perry et al., 2018). Esta medida se define como el número de alter diferentes de ego (lazos externos E) menos el número de alter iguales a ego (lazos internos I), dividido por el número de alter. Esta es una medida "reversa" de homofilia toa vez que una medida alta de este índice índica mayor heterofilia. Además, debido a que es una transformación lineal de la medida de proporción de lazos homofilicos, su correlación es un perfecto -.1 

$$ EI = {\frac {E - I}{E + I}}$$

A continuación, desarrollamos el trabajo de código. Todo el análisis es realizado con datos de la encuesta ELSOC COES, considerando las olas 2 y 4. 


# Cargar librerias y bbdd

```
pacman::p_load(ggplot2, 
               ggthemes, 
               tidyverse, 
               sjlabelled,
               sjPlot, 
               car, 
               vcd, 
               texreg, 
               ordinal,
               nnet, 
               MASS, 
               mlogit, 
               matrixStats, 
               expss,
               sjlabelled, 
               sjmisc, 
               tidyverse, 
               weights,
               survey)

```

# Cargamos data
```
load("/home/rober/Documents/COES ELSOC/Olas/ELSOC_W01_v3.10_R.RData")
load("/home/rober/Documents/COES ELSOC/Olas/ELSOC_W02_v2.10_R.RData")
load("/home/rober/Documents/COES ELSOC/Olas/ELSOC_W03_v1.10_R.RData")
load("/home/rober/Documents/COES ELSOC/Olas/ELSOC_W04_v1.00_R.RData")
```

# Subset 2017 
```
# Armar BBDD 2019 
ego2017 <- elsoc_2017 %>%
  dplyr::select(idencuesta,
                sexo=m0_sexo,
                sexoalter01=r13_sexo_01,
                sexoalter02=r13_sexo_02,
                sexoalter03=r13_sexo_03,
                sexoalter04=r13_sexo_04,
                sexoalter05=r13_sexo_05,
                edad=m0_edad,
                edadalter01=r13_edad_01,
                edadalter02=r13_edad_02,
                edadalter03=r13_edad_03,
                edadalter04=r13_edad_04,
                edadalter05=r13_edad_05,
                relig=m38,
                religalter01=r13_relig_01,
                religalter02=r13_relig_02,
                religalter03=r13_relig_03,
                religalter04=r13_relig_04,
                religalter05=r13_relig_05,
                educ=m01,
                educalter01=r13_educ_01,
                educalter02=r13_educ_02,
                educalter03=r13_educ_03,
                educalter04=r13_educ_04,
                educalter05=r13_educ_05,
                ideol=c15,
                ideolalter01=r13_ideol_01,
                ideolalter02=r13_ideol_02,
                ideolalter03=r13_ideol_03,
                ideolalter04=r13_ideol_04,
                ideolalter05=r13_ideol_05,
                c08_01,
                c08_02,
                c08_03,
                c08_04,
                ponderador01) 

```

# Revisar BBDD
```
ego2017 <- data.frame(ego2017)
#head(ego2017)
dim(ego2017)
# Definir NA's
ego2017[ego2017=="-999"] <- NA
ego2017[ego2017=="-888"] <- NA
```

# Recodificación de variables 2017 

## Sexo
```
sexolab<-c("hombre","mujer")
ego2017$sexo_h<-factor(Recode(ego2017$sexo,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2017$alterSexo1<-factor(Recode(ego2017$sexoalter01,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2017$alterSexo2<-factor(Recode(ego2017$sexoalter02,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2017$alterSexo3<-factor(Recode(ego2017$sexoalter03,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2017$alterSexo4<-factor(Recode(ego2017$sexoalter04,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2017$alterSexo5<-factor(Recode(ego2017$sexoalter05,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
with(ego2017, summary(cbind(alterSexo1,alterSexo2,alterSexo3,alterSexo4,alterSexo5)))
```

## Edad
```
edadlab <- c("18-24", "25-34", "35-44", "45-54", "55-64", ">65")
ego2017$edadR<-factor(Recode(ego2017$edad,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)

ego2017$alterAge1<-factor(Recode(ego2017$edadalter01,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2017$alterAge2<-factor(Recode(ego2017$edadalter02,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2017$alterAge3<-factor(Recode(ego2017$edadalter03,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2017$alterAge4<-factor(Recode(ego2017$edadalter04,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2017$alterAge5<-factor(Recode(ego2017$edadalter05,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
with(ego2017, summary(cbind(alterAge1,alterAge2,alterAge3,alterAge4,alterAge5)))
```

## Religión
```
rellab = c("Catolico","Evangelico","Otra Religion","no religioso")
ego2017$religid<-factor(Recode(ego2017$relig,"1=1;2=2;3:6=3;7:9=4;-999:-888=4"),labels=rellab)
ego2017$alterRelig1<-factor(Recode(ego2017$religalter01,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2017$alterRelig2<-factor(Recode(ego2017$religalter02,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2017$alterRelig3<-factor(Recode(ego2017$religalter03,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2017$alterRelig4<-factor(Recode(ego2017$religalter04,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2017$alterRelig5<-factor(Recode(ego2017$religalter05,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
with(ego2017, summary(cbind(alterRelig1,alterRelig2,alterRelig3,alterRelig4,alterRelig5)))
```

## Educación
```
edulab = c("Basica", "Media", "Sup. Tecnica", "Sup. Univ")
ego2017$educaF<-factor(Recode(ego2017$educ,"1:3=1;4:5=2;6:7=3;8:10=4;-888=NA;-999=NA"),labels=edulab)
ego2017$alterEduca1<-factor(Recode(ego2017$educalter01,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2017$alterEduca2<-factor(Recode(ego2017$educalter02,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2017$alterEduca3<-factor(Recode(ego2017$educalter03,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2017$alterEduca4<-factor(Recode(ego2017$educalter04,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2017$alterEduca5<-factor(Recode(ego2017$educalter05,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
with(ego2017, summary(cbind(alterEduca1,alterEduca2,alterEduca3,alterEduca4,alterEduca5)))
```

## Posición Política
```
pollab <- c("Izq", "Centro", "Der", "Ind/Ninguno", "NA")
ego2017$izqderR<-factor(Recode(ego2017$ideol,"0:3=1;4:6=2;7:10=3;11:12=4;NA=5"),labels=pollab)
ego2017$alterPospol1=factor(Recode(ego2017$ideolalter01,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2017$alterPospol2=factor(Recode(ego2017$ideolalter02,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2017$alterPospol3=factor(Recode(ego2017$ideolalter03,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2017$alterPospol4=factor(Recode(ego2017$ideolalter04,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2017$alterPospol5=factor(Recode(ego2017$ideolalter05,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
with(ego2017,summary(cbind(alterPospol1,alterPospol2,alterPospol3,alterPospol4,alterPospol5)))
```

# Cálculo E-I homofilia por posición política (ELSOC 2017)
## Indice E-I de Krackhardt y Stern (1988).

Creamos  los alteres como externos (con otras categorías distinta a la del ego) o como internos (misma categoría que el ego). Los valores de -1 implican perfecta homofilia y los valores de +1 corresponden a perfecta heterofilia.

### EI posición política
```
ego2017$pospol_alt1_clasif<-ifelse(ego2017$izqderR==ego2017$alterPospol1,"External", "Internal")
ego2017$pospol_alt2_clasif<-ifelse(ego2017$izqderR==ego2017$alterPospol2,"External", "Internal")
ego2017$pospol_alt3_clasif<-ifelse(ego2017$izqderR==ego2017$alterPospol3,"External", "Internal")
ego2017$pospol_alt4_clasif<-ifelse(ego2017$izqderR==ego2017$alterPospol4,"External", "Internal")
ego2017$pospol_alt5_clasif<-ifelse(ego2017$izqderR==ego2017$alterPospol5,"External", "Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2017$pospol_external<-count_row_if(criterion = "External", 
                                        ego2017$pospol_alt1_clasif, 
                                        ego2017$pospol_alt2_clasif,
                                        ego2017$pospol_alt3_clasif, 
                                        ego2017$pospol_alt4_clasif, 
                                        ego2017$pospol_alt5_clasif) 

ego2017$pospol_internal<-count_row_if(criterion = "Internal", 
                                        ego2017$pospol_alt1_clasif, 
                                        ego2017$pospol_alt2_clasif,
                                        ego2017$pospol_alt3_clasif, 
                                        ego2017$pospol_alt4_clasif, 
                                        ego2017$pospol_alt5_clasif) 

#Finalmente, calculamos el indicador EI
ego2017$EI_index_pospol<-(ego2017$pospol_external-ego2017$pospol_internal)/
                         (ego2017$pospol_external+ego2017$pospol_internal)

summary(ego2017$EI_index_pospol)
```

### EI religión
```
ego2017$alterRelig1_clasif<-ifelse(ego2017$religid==ego2017$alterRelig1,"External","Internal")
ego2017$alterRelig2_clasif<-ifelse(ego2017$religid==ego2017$alterRelig2,"External","Internal")
ego2017$alterRelig3_clasif<-ifelse(ego2017$religid==ego2017$alterRelig3,"External","Internal")
ego2017$alterRelig4_clasif<-ifelse(ego2017$religid==ego2017$alterRelig4,"External","Internal")
ego2017$alterRelig5_clasif<-ifelse(ego2017$religid==ego2017$alterRelig5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2017$relig_external<-count_row_if(criterion = "External", 
                                     ego2017$alterRelig1_clasif, 
                                     ego2017$alterRelig2_clasif,
                                     ego2017$alterRelig3_clasif, 
                                     ego2017$alterRelig4_clasif, 
                                     ego2017$alterRelig5_clasif) 

ego2017$relig_internal<-count_row_if(criterion = "Internal", 
                                     ego2017$alterRelig1_clasif, 
                                     ego2017$alterRelig2_clasif,
                                     ego2017$alterRelig3_clasif, 
                                     ego2017$alterRelig4_clasif, 
                                     ego2017$alterRelig5_clasif) 

#Finalmente, calculamos el indicador EI
ego2017$EI_index_relig<-(ego2017$relig_external-ego2017$relig_internal)/
                        (ego2017$relig_external+ego2017$relig_internal)

summary(ego2017$EI_index_relig)
```

### EI educación
```
ego2017$alterEduca1_clasif<-ifelse(ego2017$educaF==ego2017$alterEduca1,"External","Internal")
ego2017$alterEduca2_clasif<-ifelse(ego2017$educaF==ego2017$alterEduca2,"External","Internal")
ego2017$alterEduca3_clasif<-ifelse(ego2017$educaF==ego2017$alterEduca3,"External","Internal")
ego2017$alterEduca4_clasif<-ifelse(ego2017$educaF==ego2017$alterEduca4,"External","Internal")
ego2017$alterEduca5_clasif<-ifelse(ego2017$educaF==ego2017$alterEduca5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2017$educ_external<-count_row_if(criterion = "External", 
                                    ego2017$alterEduca1_clasif, 
                                    ego2017$alterEduca2_clasif,
                                    ego2017$alterEduca3_clasif, 
                                    ego2017$alterEduca4_clasif, 
                                    ego2017$alterEduca5_clasif) 

ego2017$educ_internal<-count_row_if(criterion = "Internal", 
                                    ego2017$alterEduca1_clasif, 
                                    ego2017$alterEduca2_clasif,
                                    ego2017$alterEduca3_clasif, 
                                    ego2017$alterEduca4_clasif, 
                                    ego2017$alterEduca5_clasif) 

#Finalmente, calculamos el indicador EI
ego2017$EI_index_educ<-(ego2017$educ_external-ego2017$educ_internal)/
                        (ego2017$educ_external+ego2017$educ_internal)

summary(ego2017$EI_index_educ)
```

## EI Sexo
```
ego2017$alterSexo1_clasif<-ifelse(ego2017$sexo_h==ego2017$alterSexo1,"External","Internal")
ego2017$alterSexo2_clasif<-ifelse(ego2017$sexo_h==ego2017$alterSexo2,"External","Internal")
ego2017$alterSexo3_clasif<-ifelse(ego2017$sexo_h==ego2017$alterSexo3,"External","Internal")
ego2017$alterSexo4_clasif<-ifelse(ego2017$sexo_h==ego2017$alterSexo4,"External","Internal")
ego2017$alterSexo5_clasif<-ifelse(ego2017$sexo_h==ego2017$alterSexo5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2017$sexo_external<-count_row_if(criterion = "External", 
                                    ego2017$alterSexo1_clasif, 
                                    ego2017$alterSexo2_clasif,
                                    ego2017$alterSexo3_clasif, 
                                    ego2017$alterSexo4_clasif, 
                                    ego2017$alterSexo5_clasif) 

ego2017$sexo_internal<-count_row_if(criterion = "Internal", 
                                    ego2017$alterSexo1_clasif, 
                                    ego2017$alterSexo2_clasif,
                                    ego2017$alterSexo3_clasif, 
                                    ego2017$alterSexo4_clasif, 
                                    ego2017$alterSexo5_clasif) 

#Finalmente, calculamos el indicador EI
ego2017$EI_index_sexo<-(ego2017$sexo_external-ego2017$sexo_internal)/
                       (ego2017$sexo_external+ego2017$sexo_internal)

summary(ego2017$EI_index_sexo)
```

### EI edad
```
ego2017$alterAge1_clasif<-ifelse(ego2017$edadR==ego2017$alterAge1,"External","Internal")
ego2017$alterAge2_clasif<-ifelse(ego2017$edadR==ego2017$alterAge2,"External","Internal")
ego2017$alterAge3_clasif<-ifelse(ego2017$edadR==ego2017$alterAge3,"External","Internal")
ego2017$alterAge4_clasif<-ifelse(ego2017$edadR==ego2017$alterAge4,"External","Internal")
ego2017$alterAge5_clasif<-ifelse(ego2017$edadR==ego2017$alterAge5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2017$age_external<-count_row_if(criterion = "External", 
                                   ego2017$alterAge1_clasif, 
                                   ego2017$alterAge2_clasif,
                                   ego2017$alterAge3_clasif, 
                                   ego2017$alterAge4_clasif, 
                                   ego2017$alterAge5_clasif) 

ego2017$age_internal<-count_row_if(criterion = "Internal", 
                                   ego2017$alterAge1_clasif, 
                                   ego2017$alterAge2_clasif,
                                   ego2017$alterAge3_clasif, 
                                   ego2017$alterAge4_clasif, 
                                   ego2017$alterAge5_clasif) 

#Finalmente, calculamos el indicador EI
ego2017$EI_index_age<-(ego2017$age_external-ego2017$age_internal)/
                      (ego2017$age_external+ego2017$age_internal)

summary(ego2017$EI_index_age)
```

# Subset 2019 
```
# Armar BBDD 2019 
ego2019 <- elsoc_2019 %>%
  dplyr::select(idencuesta,
                sexo=m0_sexo,
                sexoalter01=r13_sexo_01,
                sexoalter02=r13_sexo_02,
                sexoalter03=r13_sexo_03,
                sexoalter04=r13_sexo_04,
                sexoalter05=r13_sexo_05,
                edad=m0_edad,
                edadalter01=r13_edad_01,
                edadalter02=r13_edad_02,
                edadalter03=r13_edad_03,
                edadalter04=r13_edad_04,
                edadalter05=r13_edad_05,
                relig=m38,
                religalter01=r13_relig_01,
                religalter02=r13_relig_02,
                religalter03=r13_relig_03,
                religalter04=r13_relig_04,
                religalter05=r13_relig_05,
                educ=m01,
                educalter01=r13_educ_01,
                educalter02=r13_educ_02,
                educalter03=r13_educ_03,
                educalter04=r13_educ_04,
                educalter05=r13_educ_05,
                ideol=c15,
                ideolalter01=r13_ideol_01,
                ideolalter02=r13_ideol_02,
                ideolalter03=r13_ideol_03,
                ideolalter04=r13_ideol_04,
                ideolalter05=r13_ideol_05,
                c08_01,
                c08_02,
                c08_03,
                c08_04,
                ponderador01) 

```


# Revisar BBDD
```
ego2019 <- data.frame(ego2019)
#head(ego2019)
dim(ego2019)
# Definir NA's
ego2019[ego2019=="-999"] <- NA
ego2019[ego2019=="-888"] <- NA
```

# Recod variables 2019 

## Sexo
```
sexolab<-c("hombre","mujer")
ego2019$sexo_h<-factor(Recode(ego2019$sexo,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2019$alterSexo1<-factor(Recode(ego2019$sexoalter01,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2019$alterSexo2<-factor(Recode(ego2019$sexoalter02,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2019$alterSexo3<-factor(Recode(ego2019$sexoalter03,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2019$alterSexo4<-factor(Recode(ego2019$sexoalter04,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
ego2019$alterSexo5<-factor(Recode(ego2019$sexoalter05,"1=1;2=2;-888=NA;-999=NA"),labels=sexolab)
with(ego2019, summary(cbind(alterSexo1,alterSexo2,alterSexo3,alterSexo4,alterSexo5)))
```

## Edad
```
edadlab <- c("18-24", "25-34", "35-44", "45-54", "55-64", ">65")
ego2019$edadR<-factor(Recode(ego2019$edad,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)

ego2019$alterAge1<-factor(Recode(ego2019$edadalter01,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2019$alterAge2<-factor(Recode(ego2019$edadalter02,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2019$alterAge3<-factor(Recode(ego2019$edadalter03,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2019$alterAge4<-factor(Recode(ego2019$edadalter04,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
ego2019$alterAge5<-factor(Recode(ego2019$edadalter05,"lo:24=1;25:34=2;35:44=3;45:54=4;55:64=5;65:hi=6"),labels=edadlab)
with(ego2019, summary(cbind(alterAge1,alterAge2,alterAge3,alterAge4,alterAge5)))
```

## Religión
```
rellab = c("Catolico","Evangelico","Otra Religion","no religioso")
ego2019$religid<-factor(Recode(ego2019$relig,"1=1;2=2;3:6=3;7:9=4;-999:-888=4"),labels=rellab)
ego2019$alterRelig1<-factor(Recode(ego2019$religalter01,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2019$alterRelig2<-factor(Recode(ego2019$religalter02,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2019$alterRelig3<-factor(Recode(ego2019$religalter03,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2019$alterRelig4<-factor(Recode(ego2019$religalter04,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
ego2019$alterRelig5<-factor(Recode(ego2019$religalter05,"1=1;2=2;3:4=4;5=3;-999:-888=4"),labels=rellab)
with(ego2019, summary(cbind(alterRelig1,alterRelig2,alterRelig3,alterRelig4,alterRelig5)))
```

## Educación
```
edulab = c("Basica", "Media", "Sup. Tecnica", "Sup. Univ")
ego2019$educaF<-factor(Recode(ego2019$educ,"1:3=1;4:5=2;6:7=3;8:10=4;-888=NA;-999=NA"),labels=edulab)
ego2019$alterEduca1<-factor(Recode(ego2019$educalter01,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2019$alterEduca2<-factor(Recode(ego2019$educalter02,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2019$alterEduca3<-factor(Recode(ego2019$educalter03,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2019$alterEduca4<-factor(Recode(ego2019$educalter04,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
ego2019$alterEduca5<-factor(Recode(ego2019$educalter05,"1=1;2:3=2;4=3;5=4;-888=NA;-999=NA"),labels=edulab)
with(ego2019, summary(cbind(alterEduca1,alterEduca2,alterEduca3,alterEduca4,alterEduca5)))
```

## Posición Política
```
pollab <- c("Izq", "Centro", "Der", "Ind/Ninguno", "NA")
ego2019$izqderR<-factor(Recode(ego2019$ideol,"0:3=1;4:6=2;7:10=3;11:12=4;NA=5"),labels=pollab)
ego2019$alterPospol1=factor(Recode(ego2019$ideolalter01,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2019$alterPospol2=factor(Recode(ego2019$ideolalter02,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2019$alterPospol3=factor(Recode(ego2019$ideolalter03,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2019$alterPospol4=factor(Recode(ego2019$ideolalter04,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
ego2019$alterPospol5=factor(Recode(ego2019$ideolalter05,"1:2=3;3=2;4:5=1;6=4;NA=5"),labels=pollab)
with(ego2019,summary(cbind(alterPospol1,alterPospol2,alterPospol3,alterPospol4,alterPospol5)))
```

# Cálculo E-I homofilia por posición política (ELSOC 2019)
## Indice E-I de Krackhardt y Stern (1988).

Creamos  los alteres como externos (con otras categorías distinta a la del ego) o como internos (misma categoría que el ego). Los valores de -1 implican perfecta homofilia y los valores de +1 corresponden a perfecta heterofilia.

### EI posición política
```
ego2019$pospol_alt1_clasif<-ifelse(ego2019$izqderR==ego2019$alterPospol1,"External", "Internal")
ego2019$pospol_alt2_clasif<-ifelse(ego2019$izqderR==ego2019$alterPospol2,"External", "Internal")
ego2019$pospol_alt3_clasif<-ifelse(ego2019$izqderR==ego2019$alterPospol3,"External", "Internal")
ego2019$pospol_alt4_clasif<-ifelse(ego2019$izqderR==ego2019$alterPospol4,"External", "Internal")
ego2019$pospol_alt5_clasif<-ifelse(ego2019$izqderR==ego2019$alterPospol5,"External", "Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2019$pospol_external<-count_row_if(criterion = "External", 
                                        ego2019$pospol_alt1_clasif, 
                                        ego2019$pospol_alt2_clasif,
                                        ego2019$pospol_alt3_clasif, 
                                        ego2019$pospol_alt4_clasif, 
                                        ego2019$pospol_alt5_clasif) 

ego2019$pospol_internal<-count_row_if(criterion = "Internal", 
                                        ego2019$pospol_alt1_clasif, 
                                        ego2019$pospol_alt2_clasif,
                                        ego2019$pospol_alt3_clasif, 
                                        ego2019$pospol_alt4_clasif, 
                                        ego2019$pospol_alt5_clasif) 

#Finalmente, calculamos el indicador EI
ego2019$EI_index_pospol<-(ego2019$pospol_external-ego2019$pospol_internal)/
                         (ego2019$pospol_external+ego2019$pospol_internal)

summary(ego2019$EI_index_pospol)
```

### EI religión
```
ego2019$alterRelig1_clasif<-ifelse(ego2019$religid==ego2019$alterRelig1,"External","Internal")
ego2019$alterRelig2_clasif<-ifelse(ego2019$religid==ego2019$alterRelig2,"External","Internal")
ego2019$alterRelig3_clasif<-ifelse(ego2019$religid==ego2019$alterRelig3,"External","Internal")
ego2019$alterRelig4_clasif<-ifelse(ego2019$religid==ego2019$alterRelig4,"External","Internal")
ego2019$alterRelig5_clasif<-ifelse(ego2019$religid==ego2019$alterRelig5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2019$relig_external<-count_row_if(criterion = "External", 
                                     ego2019$alterRelig1_clasif, 
                                     ego2019$alterRelig2_clasif,
                                     ego2019$alterRelig3_clasif, 
                                     ego2019$alterRelig4_clasif, 
                                     ego2019$alterRelig5_clasif) 

ego2019$relig_internal<-count_row_if(criterion = "Internal", 
                                     ego2019$alterRelig1_clasif, 
                                     ego2019$alterRelig2_clasif,
                                     ego2019$alterRelig3_clasif, 
                                     ego2019$alterRelig4_clasif, 
                                     ego2019$alterRelig5_clasif) 

#Finalmente, calculamos el indicador EI
ego2019$EI_index_relig<-(ego2019$relig_external-ego2019$relig_internal)/
                        (ego2019$relig_external+ego2019$relig_internal)

summary(ego2019$EI_index_relig)
```

### EI educación
```
ego2019$alterEduca1_clasif<-ifelse(ego2019$educaF==ego2019$alterEduca1,"External","Internal")
ego2019$alterEduca2_clasif<-ifelse(ego2019$educaF==ego2019$alterEduca2,"External","Internal")
ego2019$alterEduca3_clasif<-ifelse(ego2019$educaF==ego2019$alterEduca3,"External","Internal")
ego2019$alterEduca4_clasif<-ifelse(ego2019$educaF==ego2019$alterEduca4,"External","Internal")
ego2019$alterEduca5_clasif<-ifelse(ego2019$educaF==ego2019$alterEduca5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2019$educ_external<-count_row_if(criterion = "External", 
                                    ego2019$alterEduca1_clasif, 
                                    ego2019$alterEduca2_clasif,
                                    ego2019$alterEduca3_clasif, 
                                    ego2019$alterEduca4_clasif, 
                                    ego2019$alterEduca5_clasif) 

ego2019$educ_internal<-count_row_if(criterion = "Internal", 
                                    ego2019$alterEduca1_clasif, 
                                    ego2019$alterEduca2_clasif,
                                    ego2019$alterEduca3_clasif, 
                                    ego2019$alterEduca4_clasif, 
                                    ego2019$alterEduca5_clasif) 

#Finalmente, calculamos el indicador EI
ego2019$EI_index_educ<-(ego2019$educ_external-ego2019$educ_internal)/
                        (ego2019$educ_external+ego2019$educ_internal)

summary(ego2019$EI_index_educ)

```

## EI Sexo
```
ego2019$alterSexo1_clasif<-ifelse(ego2019$sexo_h==ego2019$alterSexo1,"External","Internal")
ego2019$alterSexo2_clasif<-ifelse(ego2019$sexo_h==ego2019$alterSexo2,"External","Internal")
ego2019$alterSexo3_clasif<-ifelse(ego2019$sexo_h==ego2019$alterSexo3,"External","Internal")
ego2019$alterSexo4_clasif<-ifelse(ego2019$sexo_h==ego2019$alterSexo4,"External","Internal")
ego2019$alterSexo5_clasif<-ifelse(ego2019$sexo_h==ego2019$alterSexo5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2019$sexo_external<-count_row_if(criterion = "External", 
                                    ego2019$alterSexo1_clasif, 
                                    ego2019$alterSexo2_clasif,
                                    ego2019$alterSexo3_clasif, 
                                    ego2019$alterSexo4_clasif, 
                                    ego2019$alterSexo5_clasif) 

ego2019$sexo_internal<-count_row_if(criterion = "Internal", 
                                    ego2019$alterSexo1_clasif, 
                                    ego2019$alterSexo2_clasif,
                                    ego2019$alterSexo3_clasif, 
                                    ego2019$alterSexo4_clasif, 
                                    ego2019$alterSexo5_clasif) 

#Finalmente, calculamos el indicador EI
ego2019$EI_index_sexo<-(ego2019$sexo_external-ego2019$sexo_internal)/
                       (ego2019$sexo_external+ego2019$sexo_internal)

summary(ego2019$EI_index_sexo)
```

### EI edad
```
ego2019$alterAge1_clasif<-ifelse(ego2019$edadR==ego2019$alterAge1,"External","Internal")
ego2019$alterAge2_clasif<-ifelse(ego2019$edadR==ego2019$alterAge2,"External","Internal")
ego2019$alterAge3_clasif<-ifelse(ego2019$edadR==ego2019$alterAge3,"External","Internal")
ego2019$alterAge4_clasif<-ifelse(ego2019$edadR==ego2019$alterAge4,"External","Internal")
ego2019$alterAge5_clasif<-ifelse(ego2019$edadR==ego2019$alterAge5,"External","Internal")

#Ahora se agrega la información sobre los tipos de vínculos
ego2019$age_external<-count_row_if(criterion = "External", 
                                   ego2019$alterAge1_clasif, 
                                   ego2019$alterAge2_clasif,
                                   ego2019$alterAge3_clasif, 
                                   ego2019$alterAge4_clasif, 
                                   ego2019$alterAge5_clasif) 

ego2019$age_internal<-count_row_if(criterion = "Internal", 
                                   ego2019$alterAge1_clasif, 
                                   ego2019$alterAge2_clasif,
                                   ego2019$alterAge3_clasif, 
                                   ego2019$alterAge4_clasif, 
                                   ego2019$alterAge5_clasif) 

#Finalmente, calculamos el indicador EI
ego2019$EI_index_age<-(ego2019$age_external-ego2019$age_internal)/
                      (ego2019$age_external+ego2019$age_internal)

summary(ego2019$EI_index_age)
```

# Construir índice de activismo
```
ego2017<-ego2017%>%
  mutate(act=(c08_01+c08_02+c08_03+c08_04)/4)
ego2019<-ego2019%>%
  mutate(act=(c08_01+c08_02+c08_03+c08_04)/4)
```

# Construir data frame en formato Long
```
a<-ego2017%>%
  dplyr::select(idencuesta,
                sexo,
                edad,
                educ,
                act,
                EI_index_pospol,
                EI_index_relig,
                EI_index_educ,
                EI_index_sexo,
                EI_index_age,
                ponderador01)

b<-ego2019%>%
  dplyr::select(idencuesta,
                sexo,
                edad,
                educ,
                act,
                EI_index_pospol,
                EI_index_relig,
                EI_index_educ,
                EI_index_sexo,
                EI_index_age,
                ponderador01)

# Generar identificador de ola y setear bases
a$ola<-1
b$ola<-2

elsoc_w1w3<-rbind(a,b)
elsoc_w1w3<-arrange(elsoc_w1w3, idencuesta) 

# Guardar base en formato long
save(elsoc_w1w3, file = "elsoc_longEI.RData")
```

# Ttest
```
t<-wtd.t.test(x=elsoc_w1w3$EI_index_age[elsoc_w1w3$ola=="1"],
              y=elsoc_w1w3$EI_index_age[elsoc_w1w3$ola=="2"],
              weight= elsoc_w1w3$ponderador01[elsoc_w1w3$ola=="1"],
              weighty=elsoc_w1w3$ponderador01[elsoc_w1w3$ola=="2"],
              samedata=T,alternative="two.tailed")
t
#?wtd.t.test
```

# Análisis panel (within-between) longitudinal 
```
library(panelr)
elsoc_w1w3<-panel_data(data=elsoc_w1w3, id = idencuesta, wave = ola)
elsoc_w1w3<-complete_data(data = elsoc_w1w3, min.waves = 2)

# Modelos within-between
modelo1 <- wbm(EI_index_pospol~act|sexo+edad+educ,data=elsoc_w1w3)
summary(modelo1) 
```



