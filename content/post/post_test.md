---
title: "Descriptivos y Distancia sociodemográfica ego-network ELSOC-COES"
author: "RedesLab"  
date: "2021-08-23"
update: "2021-08-23"
tags: ["Redes Sociales", "Redes Egocentricas", "Generador de nombres", "R"]
---
El presente documento tiene por objetivo realizar la construcción de una Base de Datos de egos y alteris (anidados) en 
formato long para análisis posteriores considerando ELSOC 2017 (Ola 2) -esto eventualmente puede ser extendido a ELSOC 
2019- Términamos con la construcción de tablas de descriptivos y la construcción de vectores de "distancia 
sociodemográfica" (mismatch) de parámetros de relevancia evidenciados en el paper de Bargsted et al. (2020) para el caso 
chileno. 
Para el cálculo de esta medida, utilizaremos la base de datos longitudinal, ELSOC (ola 2), en formato R la cual puede 
ser descargada en el siguiente [link](https://www.dataverse.harvard.edu/file.xhtml?persistentId=doi:10.7910/DVN/JDJLPQ/CJJV3T&version=3.0). 
Lo invitamos también a poder revisar el repositorio https://www.dataverse.harvard.edu/dataverse/coes_data_repository donde 
podrá encontrar disponible los datos de 2017, 2018 de la encuesta ELSOC en diversos formatos, así como también su 
cuestionario. En este link usted podrá también podrá encontrar las bases de datos corresponientes a otras encuestas 
realizadas por el centro.
Para más información de COES o ELSOC, lo invitamos a revisar https://www.coes.cl/ y https://www.elsoc.cl/

Para el siguiente ejemplo usamos data egocentrada siguiendo el formato que se utiliza para manipulación de data con el paquete `egor`

# Librerias
```
library(ggplot2)
library(ggthemes)
library(tidyverse)
library(sjlabelled)
library(sjPlot)
library(vcd)
library(texreg)
library(ordinal)
library(nnet)
library(MASS)
library(mlogit)
library(matrixStats)
library(expss)
library(sjlabelled)
library(sjmisc)
library(tidyverse)
library(egor)
library(haven)
library(car)
library(dplyr)
library(stargazer)
library(janitor)
library(haven)
libraRY(skimr)
library(weights)
library(ggcorrplot)
library(ggridges)
library(panelr)
```
# Cargamos data 2017 
```
load("C:/Users/Herman/Desktop/ETC/ELSOC_W02_v3.00_R.Rdata")
load("C:/Users/Herman/Desktop/ETC/ELSOC_W04_v2.01_R.RData")
```

# Renombrar ID

```{r}
a<-elsoc_2017 %>% rename(.egoID = idencuesta)
b<-elsoc_2019 %>% rename(.egoID = idencuesta)
```

# Crear data frame alteris para 2017=a1

Creamos subset con data de cada uno de los alteris mencionados, manteniendo el ID de cada ego en el cual están anidados. Las columnas de cada uno de los subset deben tener los mismos nombres. 

```
alter_1<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_01, 
                      alter_edad=r13_edad_01, 
                      alter_rel=r13_relacion_01,
                      alter_tiempo=r13_tiempo_01,
                      alter_barrio=r13_barrio_01, 
                      alter_educ=r13_educ_01, 
                      alter_relig=r13_relig_01, 
                      alter_ideol=r13_ideol_01)

alter_2<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_02, 
                      alter_edad=r13_edad_02, 
                      alter_rel=r13_relacion_02,
                      alter_tiempo=r13_tiempo_02,
                      alter_barrio=r13_barrio_02, 
                      alter_educ=r13_educ_02, 
                      alter_relig=r13_relig_02, 
                      alter_ideol=r13_ideol_02)

alter_3<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_03, 
                      alter_edad=r13_edad_03, 
                      alter_rel=r13_relacion_03,
                      alter_tiempo=r13_tiempo_03,
                      alter_barrio=r13_barrio_03, 
                      alter_educ=r13_educ_03, 
                      alter_relig=r13_relig_03, 
                      alter_ideol=r13_ideol_03)

alter_4<- a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_04, 
                      alter_edad=r13_edad_04, 
                      alter_rel=r13_relacion_04,
                      alter_tiempo=r13_tiempo_04, 
                      alter_barrio=r13_barrio_04, 
                      alter_educ=r13_educ_04, 
                      alter_relig=r13_relig_04, 
                      alter_ideol=r13_ideol_04)

alter_5<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_05, 
                      alter_edad=r13_edad_05, 
                      alter_rel=r13_relacion_05,
                      alter_tiempo=r13_tiempo_05, 
                      alter_barrio=r13_barrio_05, 
                      alter_educ=r13_educ_05, 
                      alter_relig=r13_relig_05, 
                      alter_ideol=r13_ideol_05)
```

# setear

Creamos un vector adicional en cada subset de alteris con un número constante que identifica a que alter representa la data. 

```
alter_1$n<-1
alter_2$n<-2
alter_3$n<-3
alter_4$n<-4
alter_5$n<-5
```

# Crear base en formato *long*

Con la función `rbind` agregamos la data hacia abajo en relación al orden establecido por el los vectores númericos creados anteriormente. Es necesario que todas las columnas (variables) tengan los mismos nombres. Posteriormente con la función `arrange`, ordenamos la data en orden descendente en función del vector identificador de los egos (respondentes).  

```
alteris<-rbind(alter_1,alter_2,alter_3,alter_4,alter_5)
alteris<-arrange(alteris, .egoID)
```


# Crear vector alter id

En el siguiente chunk creamos un vector identificador para cada uno de los alteris presentes en la data "alteris". Lo identificamos como objeto `tibble` y eliminamos el vector "n". 

```
alteris   <- rowid_to_column(alteris, var = ".altID")
alteris   <- as_tibble(alteris)
#alteris$n <- NULL
```

# Recod alteris

Recodificamos los valores de los atributos de los alteris. 

```
alteris$alter_educ <-factor(Recode(alteris$alter_educ ,"1=1;2:3=2;4=3;5=4;-999=NA"))
alteris$alter_relig<-factor(Recode(alteris$alter_relig,"1=1;2=2;3=3;4=4;5=5;-999=NA"))
alteris$alter_ideol<-factor(Recode(alteris$alter_ideol,"1=1;2=2;3=3;4=4;5=5;6=6;-999=NA"))
alteris$alter_edad <-factor(Recode(alteris$alter_edad ,"0:18=1;19:29=2;30:40=3;41:51=4;52:62=5;63:100=6"))
alteris$alter_sexo <-factor(Recode(alteris$alter_sexo ,"1=1;2=2"))
#alteris<-na.omit(alteris)
```

# Data Frame Ego’s

Creamos un subset con la data de ego equivalente a la data de los alteris. Las nombramos de la misma manera. 

```
egos <-a %>%
       dplyr::select(.egoID, 
                     ego_sexo=m0_sexo, 
                     ego_edad=m0_edad, 
                     ego_ideol=c15, 
                     ego_educ=m01, 
                     ego_relig=m38, 
                     ego_ideol=c15)

egos <- as_tibble(egos)
```

# Recod data Ego's

Recodificamos las variables de la data de ego siguiendo el patrón de la data de alteris. 

```
egos$ego_educ <-factor(Recode(egos$ego_educ,"1:3=1;4:5=2;6:7=3;8:10=4;-999:-888=NA"))
egos$ego_relig<-factor(Recode(egos$ego_relig,"1=1;2=2;9=3;7:8=4;3:6=5;-999:-888=NA"))
egos$ego_ideol<-factor(Recode(egos$ego_ideol,"9:10=1;6:8=2;5=3;2:4=4;0:1=5;11:12=6;-999:-888=NA"))
egos$ego_edad <-factor(Recode(egos$ego_edad,"18=1;19:29=2;30:40=3;41:51=4;52:62=5;63:100=6"))
egos$ego_sexo <-factor(Recode(egos$ego_sexo,"1=1;2=2"))
```

# join

Con la función `left_join` agregamos la data de alteris y egos hacia el lado, en función del id de ego. 

```
obs<-left_join(alteris,egos, by=".egoID")
obs$case<-1
obs[obs=="-999"] <- NA
obs[obs=="-888"] <- NA
```


# Descriptivos (alter)

Observamos la frecuencia de las categorias de los atributos de alteris. 

```
freq(obs$alter_educ)
freq(obs$alter_relig)
freq(obs$alter_ideol)
freq(obs$alter_edad)
freq(obs$alter_sexo)
```

# Descriptivos (ego)

Observamos la frecuencia de las categorias de los atributos sociodemográficos de ego. 

```
freq(obs$ego_educ)
freq(obs$ego_relig)
freq(obs$ego_ideol)
freq(obs$ego_edad)
freq(obs$ego_sexo)
```


# Croos tab educ

Creamos una tabla cruzada de las categorías educativas de Ego y alteris para observar su distribución. 

```
table_cont<-sjPlot::tab_xtab(var.row = obs$ego_educ, 
                             var.col = obs$alter_educ, 
                             title = "Mixtura social por nivel educativo ELSOC 2017", 
                             show.row.prc = TRUE)
table_cont
```

# Croos tab Relig

```
table_cont2<-sjPlot::tab_xtab(var.row = obs$ego_relig, 
                             var.col = obs$alter_relig, 
                             title = "Mixtura social por religión ELSOC 2017", 
                             show.row.prc = TRUE)
table_cont2
```

# Croos tab Sexo

```
table_cont3<-sjPlot::tab_xtab(var.row = obs$ego_sexo, 
                             var.col = obs$alter_sexo, 
                             title = "Mixtura social por sexo ELSOC 2017", 
                             show.row.prc = TRUE)
table_cont3
```

# Croos tab edad

```
table_cont4<-sjPlot::tab_xtab(var.row = obs$ego_edad, 
                             var.col = obs$alter_edad, 
                             title = "Mixtura social por edad ELSOC 2017", 
                             show.row.prc = TRUE)
table_cont4
```

# Croos tab ideol

```
table_cont5<-sjPlot::tab_xtab(var.row = obs$ego_ideol, 
                             var.col = obs$alter_ideol, 
                             title = "Mixtura social por ideol ELSOC 2017", 
                             show.row.prc = TRUE)
table_cont5
```

# Tabla para plot (con ggplot en formato long)

El siguiente código crea una tabla long para ser ploteada como un heatmap en ggplot 

# Tabla educ

```
table<-as.data.frame(prop.table(table(obs$ego_educ,obs$alter_educ)),margin=1)
colnames(table)<-c("Ego_educ", "Alter_educ", "Prop")
#print(addmargins(table*100,1),3)
#table
```

## Heatmap educ

```
ggplot(table,aes(Ego_educ, Alter_educ))+
  geom_tile(aes(fill=Prop))+
  scale_fill_gradient(low="white", high="black") +
  theme_ipsum()
```
![E-I sexo](http://imgfz.com/i/KeJiD8u.png) 

# Tabla relig

```
table2<-as.data.frame(prop.table(table(obs$ego_relig,obs$alter_relig)),margin=1)
colnames(table2)<-c("Ego_relig", "Alter_relig", "Prop")
#print(addmargins(table*100,1),3)
#table
```

# Heatmap relig

```
ggplot(table2,aes(Ego_relig, Alter_relig))+
  geom_tile(aes(fill=Prop))+
  scale_fill_gradient(low="white", high="black") +
  theme_ipsum()
```
![E-I sexo](http://imgfz.com/i/ky9NZUt.png) 
# Mean en tiempo * tipos de relación

```
obs %>%
  summarise(
    mean.clo.esp   = mean(alter_tiempo[alter_rel=="1"], na.rm=T),
    mean.clo.hijo  = mean(alter_tiempo[alter_rel=="2"], na.rm=T),
    mean.clo.pari  = mean(alter_tiempo[alter_rel=="3"], na.rm=T),
    mean.clo.amig  = mean(alter_tiempo[alter_rel=="4"], na.rm=T),
    mean.clo.otro  = mean(alter_tiempo[alter_rel=="5"], na.rm=T),
    count.par.barr = sum((alter_rel=="3" & alter_barrio=="1"), na.rm=T))
```

# Crear vectores de distancia sociodemográfica 

# Crear un vector que sea TRUE siempre que alter tenga distinto sexo que ego 

```
obs$sexo_dist<-obs$alter_sexo != obs$ego_sexo
```

# otra alternativa para crear vector de distancia o "mismatch"

```
obs$sexo_dist <-ifelse(obs$alter_sexo ==obs$ego_sexo,  0, 1) # mismatch
obs$edad_dist <-ifelse(obs$alter_edad ==obs$ego_edad,  0, 1) 
obs$educ_dist <-ifelse(obs$alter_educ ==obs$ego_educ,  0, 1) 
obs$ideol_dist<-ifelse(obs$alter_ideol==obs$ego_ideol, 0, 1) 
obs$relig_dist<-ifelse(obs$alter_relig==obs$ego_relig, 0, 1) 
```

# Descriptivos 

```
freq(obs$sexo_dist)
```

```
egltable(c("sexo_dist", "edad_dist", "educ_dist", "ideol_dist", "relig_dist"),
         data = obs, strict=T) 
```

Esta tabla es la misma de Smith et al. (2014). Describe la media de la distancia sociodemogrpafica por parámetro. Como alternativa, el valor TRUE del argumento `strict` de la función `egltable`, muestra los porcentajes por categoría de la variable dummy.


# Bibliografía 

- Bargsted Valdés, M. A., Espinoza, V., & Plaza, A. (2020). Pautas de Homofilia en Chile. Papers. Revista de Sociología, 105
